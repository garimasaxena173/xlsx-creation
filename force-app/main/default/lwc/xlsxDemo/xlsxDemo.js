import { LightningElement, track } from 'lwc';
//import getContactLists from "@salesforce/apex/ContactController.getContactLists";
import getAccountLists from "@salesforce/apex/LWCExampleController.getAccounts";
import getAccountLists2 from "@salesforce/apex/LWCExampleController.getAccounts2";
import getAccountLists3 from "@salesforce/apex/LWCExampleController.getAccounts3";


export default class XlsxDemo extends LightningElement {

    @track xlsHeader = []; // store all the headers of the the tables
    @track workSheetNameList = []; // store all the sheets name of the the tables
    @track xlsData = []; // store all tables data
    @track filename = "sos_demo.xlsm"; // Name of the file
    
    showSpinner = false;
    connectedCallback() {
        
        this.showSpinner = true
        getAccountLists()
        .then(result => {
            
            this.xlsFormatter(result, "Accounts");
        })
        
    }

   
    // formating the data to send as input to  xlsxMain component
    xlsFormatter(data, sheetName) {
        let Header = Object.keys(data[0]);
        this.xlsHeader.push(Header);
        this.workSheetNameList.push(sheetName);
        this.xlsData.push(data);
    }

    // calling the download function from xlsxMain.js 
    download() {
        this.template.querySelector("c-xlsx-main").download();
    }
}